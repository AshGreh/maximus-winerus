import java.awt.Color;
import java.awt.Graphics;
import java.awt.Point;
import java.util.concurrent.Callable;

import org.powerbot.script.Condition;
import org.powerbot.script.PaintListener;
import org.powerbot.script.PollingScript;
import org.powerbot.script.Random;
import org.powerbot.script.Script;
import org.powerbot.script.Tile;
import org.powerbot.script.rt4.ClientContext;
import org.powerbot.script.rt4.TilePath;

/**
 * Wine Making #0.1
 * 
 * @author 0xSnowman
 *
 */
@Script.Manifest(name = "Maximus-Winerus 0.1", description = "Makes wines like an animal!")

public class MaximusWinerus extends PollingScript<ClientContext> implements PaintListener {

	private AntiBan ab = new AntiBan(ctx);

	private static final int WATER_JUG = 1937, GRAPES = 1987; // 1993

	private enum STATE {
		BANKING, BANKALL, COMBINE, IDLE
	}

	private Tile starting_point;
	private TilePath to_starting_point;

	private int starting_exp;
	private int starting_lvl;
	private String status;

	@Override
	public void start() {
		starting_lvl = ctx.skills.level(7);
		starting_exp = ctx.skills.experience(7);
		starting_point = ctx.players.local().tile();
		to_starting_point = ctx.movement.newTilePath(starting_point);
	}

	protected STATE state() {
		if (ctx.players.local().tile() != starting_point) {
			to_starting_point.traverse();
		}
		if (ctx.players.local().inMotion()) {
			System.out.println("~[Player movement detected... Stopping script!]~");
			stop();
		}
		ctx.inventory.select();
		if (ctx.inventory.isEmpty()) {
			status = "Banking";
			return STATE.BANKING;
		}
		if (!ctx.inventory.id(1987).isEmpty() || !ctx.inventory.id(1937).isEmpty()) {
			status = "Making Wines";
			return STATE.COMBINE;
		}
		if (ctx.inventory.id(1987).isEmpty() || ctx.inventory.id(1937).isEmpty()) {
			status = "Banking";
			return STATE.BANKING;
		}
		return STATE.IDLE;
	}

	private void bank() {
		if (!ctx.bank.inViewport()) {
			Condition.sleep(Random.getDelay());
			switch (Random.nextInt(0, 5)) {
			case 0:
				ctx.camera.turnTo(ctx.bank.nearest());
				break;
			case 1:
				ctx.camera.turnTo(ctx.bank.nearest());
				break;
			default:
				ctx.camera.turnTo(ctx.players.iterator().next());
				break;
			}
			status = "Ajusting Viewpoint";
		}
		ctx.bank.open();
		Condition.sleep(Random.getDelay());
		status = "Withdrawing Items";

		ctx.bank.depositInventory();
		ctx.bank.withdraw(1987, 14);
		ctx.bank.withdraw(1937, 14);
		if (ctx.inventory.select().id(WATER_JUG).count() > 0 && ctx.inventory.select().id(GRAPES).count() > 0) {
			ctx.bank.close();
		}
		ctx.inventory.select();
		if (ctx.inventory.select().id(WATER_JUG).count() != 14 || ctx.inventory.select().id(GRAPES).count() != 14) {
			status = "Depositing Inventory";
			ctx.bank.depositInventory();
			bank();
		}
	}

	private void combine() {
		status = "Making Wines";
		ctx.inventory.select();
		if (ctx.players.local().animation() == -1) {
			ctx.inventory.select().id(1937).poll().interact("Use");
			ctx.inventory.select().id(1987).poll().click();

			ctx.widgets.component(162, 546).interact("Make all");
			Condition.sleep(Random.getDelay());
		}
		Condition.wait(new Callable<Boolean>() {
			@Override
			public Boolean call() throws Exception {
				Condition.sleep(Random.getDelay());
				ab.execute();
				status = "AntiBan";
				ctx.camera.turnTo(ctx.players.iterator().next());
				return ctx.inventory.select().id(GRAPES).count() == 0;
			}
		}, Random.nextInt(220, 15));
	}

	@Override
	public void poll() {
		final STATE state = state();
		switch (state) {
		case BANKING:
			bank();
			break;
		case COMBINE:
			combine();
			break;
		case IDLE:
			stop();
			break;
		default:
			break;

		}
	}

	private static final Color BACKGROUND = new Color(16, 16, 16, 123);
	private static final Color FILL = new Color(48, 225, 48, 170);

	private String formated(long time) {
		final int sec = (int) (time / 1000), h = sec / 3600, m = sec / 60 % 60, s = sec % 60;
		return (h < 10 ? "0" + h : h) + ":" + (m < 10 ? "0" + m : m) + ":" + (s < 10 ? "0" + s : s);
	}

	@Override
	public void repaint(Graphics graphics) {
		long cTime = System.currentTimeMillis();
		long elapsedTime = getRuntime();
		long delta = cTime - elapsedTime;

		graphics.setColor(BACKGROUND);
		graphics.fillRoundRect(8, 179, 243, 153, 15, 15);
		graphics.setColor(Color.lightGray);
		graphics.draw3DRect(13, 294, 231, 15, true);
		graphics.setColor(FILL);
		graphics.fill3DRect(14, 295, 229 / 100, 14, true);
		graphics.setColor(Color.WHITE);
		graphics.drawString("0xMaximus-Winerus 0.1", 58, 192);
		graphics.setColor(Color.magenta);
		graphics.drawString("Runtime: " + formated(getRuntime()), 13, 208);

		int level = ctx.skills.level(7);
		int leveledTimes = ctx.skills.level(7) - starting_lvl;
		String suffix = leveledTimes == 0 ? "" : " (+" + leveledTimes + ")";
		graphics.drawString("Level: " + level + suffix, 13, 225);
		int experience = ctx.skills.experience(7);
		int experienceGained = ctx.skills.experience(7) - starting_exp;
		long hourlyExperience = (long) (experienceGained / (cTime - delta));
		String text = "Experience Gained: " + experienceGained;
		graphics.drawString(text, 13, 238);
		int catches = 17;
		int hourlyCatches = (int) (catches / (cTime - delta));
		text = "Total Wines Made: " + experienceGained / 200;
		graphics.drawString(text, 13, 255);
		int experienceLeft = 100;
		int catchesLeft = 0;
		if (experienceGained > 0 && catches > 0)
			catchesLeft = experienceLeft / (experienceGained / catches);
		text = "Experience left: ";
		graphics.drawString(text, 13, 272);
		long estimation = -1L;
		if (hourlyExperience > 0L)
			estimation = (long) (experienceLeft / hourlyExperience);

		graphics.drawString("Estimated time to level up: " + estimation, 13, 287);
		graphics.drawString("%", 113, 305);
		graphics.setColor(Color.YELLOW);
		graphics.drawString("Status: " + status, 13, 325);

		long mpt = System.currentTimeMillis() - ctx.input.getPressWhen();
		if (mpt < 1000) {
			Point pPoint = ctx.input.getPressLocation();
			graphics.setColor(Color.red);
			graphics.drawOval(pPoint.x - 2, pPoint.y - 2, 4, 4);
			graphics.drawOval(pPoint.x - 9, pPoint.y - 9, 18, 18);
		}

		Point mouse = ctx.input.getLocation();
		graphics.setColor(Color.MAGENTA);
		graphics.drawOval(mouse.x - 2, mouse.y - 2, 4, 4);
		graphics.drawOval(mouse.x - 9, mouse.y - 9, 18, 18);
		graphics.drawPolygon(starting_point.matrix(ctx).bounds());
	}
}