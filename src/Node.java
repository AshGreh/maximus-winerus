import org.powerbot.script.rt4.*;

import java.util.logging.Logger;

public abstract class Node<C extends ClientContext> extends ClientAccessor implements NodeStructure {

	public String name;

	public Node(C ctx) {
		super(ctx);
		Logger.getLogger("Log").info("Node has been created.");
	}
}
